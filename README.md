Created several versions of the same application, each one building upon the previous one.
You'll see 11 folders on the root, each one a independent project by itself. The idea was to
take advantage of having a small sample project to test tools and libraries with it (so the app
could serve as a baseline to compare). They were roughtly implemented in this order:

1. In-browser: This first version just loads react and babel (standalone) using scripts from the
unpkg cdn. It was the easiest and fastest way to create the first version of the app (no node
build was necessary). Took me about 8-10 hours to finnish (had to brush up on React :S, so it took
me longer than expected).
2. CRA (Create React App CLI): The easiest way to bootstrap a react app. Could've had ejected the
configuration, but there was not point. Just added a .env file to alias the modules more easily
(relative to src). Basically took the in-browser version and splitted it into several files (each
component on its own file).
3. Webpack (uses webpack 3), webpack2 (both using Babel): Created a webpack config by hand just to compare it with
Create React App. Copy pasted the src (entire React App).
4. Rollup: created another version with rollup instead of webpack (also using Babel), based on a
previous conf i had created for a library. Copy pasted the src.
5. Preact: Used the official preact-cli to see how easy it was to migrate a React App. Spoiler alert.
For Apps this small, it is very easy.
6. Mobx: Decided to try this "state manager" (they don't like to call themselves that way), in the
easiest way possible (just a singleton global store that can be imported from anyware). Basically,
the way you would use Redux (https://github.com/mobxjs/mobx/issues/300 nice discussion on the many
possible variations of using Mobx).
7. React 16 beta: Tested React 16 (which is close to be released) with both webpack and rollup.
Just change the package versions. On Rollup didn't work out of the box (on webpack it did).
8. Bublé: Tested Bublé in place of Babel, using Rollup.
9. Final version gathering all the info from the rest of prototypes.

All prototypes/versions have a package.json with the scripts to run them (yarn, npm start in most
cases). And all have a dist/build folder with the generated scripts to compare the file sizes.

To run the final version:
1. cd into the "final" folder.
2. run "yarn" or "npm i" to install the dependencies.
3. run "yarn start" (or npm start) and "yarn dev" (or npm run dev), each on a terminal window.
yarn dev runs rollup in watch mode. yarn start starts a custom server (i created for another project).
4. open localhost:8080 in the browser.
5. success.

Hightlights of the tests

* Used async await to fetch the data from the recommended lifecycle hook:
https://facebook.github.io/react/docs/react-component.html#componentdidmount
https://daveceddia.com/where-fetch-data-componentwillmount-vs-componentdidmount/

* Used the recommended way to create the components (functional components when only props are used,
classes binding this and without property initializers due to transpilation weight).
Good post about the subject (which actually recommends a third option, using the old
create-react-class, but after seeing the components created on the test, didn't seem to apply):
https://medium.com/dailyjs/we-jumped-the-gun-moving-react-components-to-es2015-class-syntax-2b2bb6f35cb3

* Didn't use lambda functions on the event handlers to avoid unnecessary recreation of them on render.
Created a re-usable functions when possible (see handleInputChange on Filter).
https://facebook.github.io/react/docs/handling-events.html

* Experimented with fluid typography (the main reason the app isn't pixel perfect)
https://www.smashingmagazine.com/2016/05/fluid-typography/

* Split the App in Components without going overboard. Most of the logic is in the Filter component.
Could've split that even further, but it seem like doing so would've been adding a level of
indirection, not something re-usable.

* Used stable ids on the custom arrays (which happened to be the original index, which in most
cases is not the right choice, but in this app's case didn't have any of the drawbacks).

* Made a good appropriate use of Command Line Tools that help to bootstrap projects. In the case of
Create React App, the point was to compare it with a hand crafted config. Could've ejected the config
and changed it, but it wasn't necessary. The project already is customizable enough that all i had
to do was to add a .env to easy imports and remove build from the gitignore (to get the final builds
on the repo).

Used preact-cli with similar purpose.

TODOs

* Unfortunately didn't have time to test redux (but i gave priority to the libraries and tools
i had not used before, like mobx).
* No documentation. There probably just 1 line of documentation in one of the rollup configs, which
needed a workaround (on React16). Hopefully most things are self-explanatory enough.
* No unit testing. Was thinking of testing Jest (Create React App everything it needs setup and in
theory you can even use it with zero config).
* A little more organization. Since i kept doing tests and changes to the different versions, probably
things could've been more organized. But since the test is timeboxed, didn't have the time.
* Linting on builds. I setup the linting with a config on the root which the editor could use, but
didn't use it at the build level.
* Targetted yarn latest (version). Not Node v4. But for the most part it should work on that version.
Or should be easily fixable.
* Would've been nice to use some new css in js libraries.

Some Findings

* preact cli can be a drop-in replacement for React, specially if used with the preact-cli.
Even the imports worked (which pointed to React but were aliased by the cli).
The file size is way smaller (27KB for the entire minified app without styles which are about 1-2KB),
but preact doesn't implement everything in the same way.
For instance the change event triggers differently (on react after each character on preact we
need to use oninput instead of onchange).
On preact-cli async await disabled, so couldn't use it.
The serve command and init command failed on a clean folder (so the project is a little buggy still).
The docs were incorrect in some instructions (had to send a PR to fix it).

* webpack
The manually made config created a build bigger in size than the one made by Create React App. So,
the CLI made a better job at that (192KB on webpack3 with everything minified, 193KB on webpack2, vs
175 by Create React App without styles).
It was barebones config. No banner, no dll, no vendor, common or code splitting, etc.
Just added scope hoisting, the latest feature of v3, which makes the bundle bigger for some reason.
Debugging, it bails out of optimizations on most files, so there isn't much to gain.

* rollup
Created a smaller bundle size than webpack. By a lot (143KB vs 175KB by CRA). It's easier to setup
but less flexible and with less features than webpack (and plugins and documentation).

* mobx
Way easier to implement than Redux. Way easier to reason about. More prone to error (at least in
the way i set it up), since everything is global. The same problems of using global vars and global
listeners/observers can probably come up. To use mobx we just annotate the Components and the
variables they depend on with decorators (or using functions if we don't want to polyfill) ala python.

* React 16
The rollup conf didn't work (had to use production workaround) on dev mode.
For the most part on both rollup and webpack it works without problems (the entire App works the
same as 15 and the source code didn't have to be changed).
The bundle size is also smaller compared to v15. 129KB with rollup and 171 with Webpack (using my
config that created a 192KB file).

* Final Version

This implementation has the smallest size (and performance due to the correlation, at least on
initial load).
Uses rollup with no state management.
Just regular event lifting and props going down.
Functional components when no classes are needed (due to only props).
Fetch instead of async await (save a big polyfill and transpilation).
Uses preact with oninput instead of onchange when necessary.
Yep, preact is not 100% 1to1 the same as React, but the migration was easy enough. At least in
this case. Minimum changes (changed imports to point to preact, added a pragma for jsx to use preact's
own function, and with webpack and an alias those changes in the source code would not have been necessary).

In the end this final version has the smallest footprint, even better than preact-cli's and all the
other combinations. The final size is 17KB (includes everything, even the styles). 13% of the best
react implementation i could come up with.
Despite using preact, the components are 100% react components and in the case of a rollback (for
any reason), the changes are minimal. Removed most things that were not necessary to improve that
final distribution size (mobx, etc).

All in all, took me about 20 hours to complete the test. 8-10 on the first in-browser interation that
included a working version of the app. Another 8 trying different variation.
And 2 cleaning up the final version and writting the conclusions.
